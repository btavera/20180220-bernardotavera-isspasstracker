//
//  WebError.swift
//  TrackISS
//
//  Created by Bernardo Tavera on 2/19/18.
//  Copyright © 2018 Bernardo Tavera. All rights reserved.
//

import Foundation

// TODO: - Complete the error cases for a more detailed scenarios

public enum WebError<CustomError>: Error {
    case noInternetConnection
    case custom(CustomError)
    case other
}
