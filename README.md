# 20180220-BernardoTavera-ISSPassTracker

Welcome to ISS-PASS tracker App Repo
-------------------------------

Coding Challenge: International Space Station Passes

The purpose of this app is to evaluate developer’s technical solution and follow instructions process

REQUIREMENTS:

1.	Read about the following ISS Pass Times API here: http://open-notify.org/Open-Notify-API/ISS-Pass-Times/
2.	Obtain the device’s GPS coordinates, if available, and pass them to the service.
3.	In a list, display each “pass” of the ISS for the given coordinates.
4.	Each item on the list should at minimum display the duration in seconds, as well as the time (convert to readable timestamp).

App notes:
•	The App is working properly according with the requirements. It has a Map to visualize the location of the device. The passes information is displayed using a Table View with two sections. The first section to display Device GPS capture information. The second section displays the detail of each one of the passes that the ISS Station did on the coordinates of the device location ( latitude and longitude )

•	The code has simplify comments for an easy understanding following the MARK and TODO indicators. It has a TODO notes where adjustments need to be done for improvement.


•	As a coding patterns the app has implemented the REST client into which the app loads a list of ISS-Passes on specific coordinates ( latitude/longitude). The remote server uses JSON for responses. The core of the REST client will be built on these following components:

Models: Classes that describe the data models .
Parsers: Responsible for decoding server responses and producing model objects.
Errors: Objects to represent erroneous server responses.
Client: Sends requests to backend servers and receives responses.


•	Unit test was implemented for the endpoint of the URLSession  for the ISS-PASSES Api. More testing need to be implemented for the data model structure as part of the TODO list for improvement

