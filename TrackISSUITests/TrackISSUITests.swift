//
//  TrackISSUITests.swift
//  TrackISSUITests
//
//  Created by Bernardo Tavera on 2/18/18.
//  Copyright © 2018 Bernardo Tavera. All rights reserved.
//

import XCTest
@testable import TrackISS

class TrackISSUITests: XCTestCase {
    
    var sessionUnderTest: URLSession!
    let lat = -48.123
    let long = 26.123
    
    override func setUp() {
        super.setUp()
        sessionUnderTest = URLSession(configuration: URLSessionConfiguration.default)
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()
        
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    // Asynchronous test: faster fail/ verify if ISS endpoint is returning statusCode 200. Verify formatting of latitude and longitude
    
    func testCallToISSTracker() {
        // given
        let url = URL(string: "http://api.open-notify.org/iss-pass.json?lat=\(lat)"+"&lon=\(long)")
        // 1
        let promise = expectation(description: "Completion handler invoked")
        var statusCode: Int?
        var responseError: Error?
        
        // when
        let dataTask = sessionUnderTest.dataTask(with: url!) { data, response, error in
            statusCode = (response as? HTTPURLResponse)?.statusCode
            responseError = error
            // 2
            promise.fulfill()
        }
        dataTask.resume()
        // 3
        waitForExpectations(timeout: 5, handler: nil)
        
        // then
        XCTAssertNil(responseError)
        XCTAssertEqual(statusCode, 200)
    }
    
}
